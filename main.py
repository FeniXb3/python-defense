import pygame
import time
import math
from Engine.WorldFactory import WorldFactory
from Engine.WorldModel import WorldModel
from Engine.TowerModel import TowerModel
from Engine.EnemyModel import EnemyModel
from Engine.AmmoModel import AmmoModel
from Engine.SpritesModel import Button
from random import random

# Import the android module. If we can't import it, set it to None - this
# lets us test it, and check to see if we want android-specific behavior.
try:
    import android
except ImportError:
    android = None

# Event constant.
TIMEREVENT = pygame.USEREVENT

# The FPS the game runs at.
FPS = 30

# Color constants.
BLACK = (0, 0, 0, 255)
GREEN = (0, 255, 0, 255)
RED = (255, 0, 0, 255)
YELLOW = (255, 255, 0, 255)
BLUE = (0, 0, 50, 255)


class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y


def getTimeInMilliseconds():
    return int(round(time.time() * 1000))

def distance(a,b):
    return math.sqrt((a.x - b.x)**2 + (a.y - b.y)**2)

def is_between(a,c,b, epsilon):
    return -epsilon < (distance(a, c) + distance(c, b) - distance(a, b)) < epsilon


def is_onPath(pathPoints, point, epsilon):
    onPath = False
    pointIndex = 0

    while pointIndex+1 < len(pathPoints) and onPath == False:
        onPath = is_between(pathPoints[pointIndex], point, pathPoints[pointIndex+1], epsilon)
        pointIndex = pointIndex+1

    return onPath

def main():
    # world = WorldModel()
    #world.test()
    #exit()
    
    pygame.init()

    # Set the screen size.
    screen = pygame.display.set_mode((480, 500))

    # Map the back button to the escape key.
    if android:
        android.init()
        android.map_key(android.KEYCODE_BACK, pygame.K_ESCAPE)

    # Use a timer to control FPS.
    pygame.time.set_timer(TIMEREVENT, 1000 / FPS)

    # The color of the screen.
    color = BLUE
    lastTime = getTimeInMilliseconds()
    worldFactory = WorldFactory()
    worldModel = worldFactory.createWorld()
    
    builder = True
    showRange = True
    play = False
    
    pressedsurf = pygame.Surface([50,50])
    pygame.draw.rect(pressedsurf,RED,pygame.Rect(0,0,50,50))
    pygame.draw.polygon(pressedsurf, GREEN, [[5, 5], [45, 25], [5, 45]], 5)


    notpressedsurf = pygame.Surface([50,50])
    pygame.draw.rect(notpressedsurf,GREEN,pygame.Rect(0,0,50,50))
    pygame.draw.rect(notpressedsurf,RED,pygame.Rect(10,5,10,40))
    pygame.draw.rect(notpressedsurf,RED,pygame.Rect(30,5,10,40))

    button = Button(370, 0, 50, 50, notpressedsurf, pressedsurf)
    allsprites = pygame.sprite.RenderPlain(button)
    buttons = pygame.sprite.RenderPlain(button)

    # for wave in worldModel.waves:
    #     #if (currentTime - worldModel.startTime) / 1000 >= wave.startTime:
    #      #   wave.started = True
    #     for enemy in wave.enemies:
    #         allsprites.add(enemy)

    while True:
        ev = pygame.event.wait()

        # Android-specific:
        if android:
            if android.check_pause():
                android.wait_for_resume()
                
        
        allsprites.update()


        
        if ev.type == pygame.MOUSEBUTTONDOWN:
            mousePos = pygame.mouse.get_pos()
            for btn in pygame.sprite.Group.sprites(buttons):
                if btn.rect.collidepoint(mousePos[0], mousePos[1]):
                    button.press()


            if(mousePos[0]>430 and mousePos[0]<500 and mousePos[1]>70 and mousePos[1]<120) :
                if showRange == True:
                    showRange = False
                else:
                    showRange = True
            elif(mousePos[0]>430 and mousePos[0]<500 and mousePos[1]>0 and mousePos[1]<50):
                if builder == True:
                    builder = False
                else:
                    builder = True

            elif(mousePos[0]>370 and mousePos[0]<410 and mousePos[1]>0 and mousePos[1]<50):
                #button.press()
                if play == True:
                    play = False
                else:
                    play = True
            elif builder == True and play == True and len(worldModel.towers) < worldModel.maxTowers:
                buildingPoint = Point(mousePos[0], mousePos[1])
                epsilon = 5
                if is_onPath(worldModel.pathPoints, buildingPoint, epsilon):
                    ammo = AmmoModel(1, 1)
                    ammo.speed = 50
                    tower = TowerModel(1, 1, 140, 1, ammo, mousePos[0], mousePos[1])
                    worldModel.towers.append(tower)
                    allsprites.add(tower)
                    #builder = False
            # else:    
            #     enemy = EnemyModel(20, random()*10+5, worldModel.pathPoints[0].x, worldModel.pathPoints[0].y,)
            #     enemy.nextPathPoint = 0
        elif ev.type == pygame.KEYDOWN and ev.key == pygame.K_ESCAPE:
            break    
        # Draw the screen based on the timer.
        elif ev.type == TIMEREVENT:
            currentTime = getTimeInMilliseconds()
            deltaTime = currentTime - lastTime
            lastTime = currentTime
            #display
            screen.fill(color)
            
            
            #engine
            if worldModel.gameOver == False and play == True:
                assert isinstance(worldModel, WorldModel)
                worldModel.fireTowers(deltaTime)

                if worldModel.startTime > 0:
                    for wave in worldModel.waves:
                        if (currentTime - worldModel.startTime) / 1000 >= wave.startTime:
                            worldModel.moveEnemies(deltaTime)
                for ammo in worldModel.ammoObjects:
                    if isinstance(ammo, AmmoModel) == False:
                        worldModel.ammoObjects.remove(ammo)
                        del ammo
                worldModel.moveAmmo(deltaTime)
                
            
            
            
            #road
            oldPoint = None;
            for pathPoint in worldModel.pathPoints:
                if oldPoint != None:
                    pygame.draw.line(screen, GREEN, (oldPoint.x, oldPoint.y), (pathPoint.x, pathPoint.y), 2)
                oldPoint = pathPoint
                if pathPoint.isGate:
                    pygame.draw.polygon(screen, GREEN, [[oldPoint.x+15, oldPoint.y + 15], [oldPoint.x+15, oldPoint.y - 15], 
                        [oldPoint.x, oldPoint.y - 20], [oldPoint.x-15, oldPoint.y - 15], [oldPoint.x-15, oldPoint.y + 15]], 2)
                

            #towers
            for tower in worldModel.towers:
                assert isinstance(tower, TowerModel)
                pygame.draw.circle(screen, GREEN, (tower.positionX, tower.positionY),5 ,0);
                tower.ammoSprites.draw(screen)
                #range
                if showRange:
                    pygame.draw.circle(screen, GREEN, (tower.positionX, tower.positionY), tower.fireRange ,1);
                
                
            #enemies

            if worldModel.startTime > 0:
                for wave in worldModel.waves:
                    if (currentTime - worldModel.startTime) / 1000 >= wave.startTime:
                        for enemy in wave.enemies:
                            assert isinstance(enemy, EnemyModel)
                            enemyColor = RED

                            pygame.draw.circle(screen, enemyColor, (math.trunc(enemy.positionX), math.trunc(enemy.positionY)),3, 0)

                            if not wave.started:
                                allsprites.add(enemy)
                        
                        wave.started = True                        

            #missles
            for missle in worldModel.ammoObjects:
                assert isinstance(missle, AmmoModel)
                pygame.draw.circle(screen, YELLOW, (math.trunc(missle.positionX), math.trunc(missle.positionY)), 2, 0)

            #builderButton
            if builder == True:
                pygame.draw.rect(screen,GREEN,pygame.Rect(430,0,50,50))
            else:
                pygame.draw.rect(screen,RED,pygame.Rect(430,0,50,50))
                
            if showRange == True:
                pygame.draw.rect(screen,GREEN,pygame.Rect(430,70,50,50))
            else:
                pygame.draw.rect(screen,RED,pygame.Rect(430,70,50,50))
        
            if play == True:
                pass
                # pygame.draw.rect(screen,GREEN,pygame.Rect(370,0,50,50))

                # pygame.draw.rect(screen,RED,pygame.Rect(380,5,10,40))
                # pygame.draw.rect(screen,RED,pygame.Rect(400,5,10,40))
            else:
                # pygame.draw.rect(screen,RED,pygame.Rect(370,0,50,50))
                # pygame.draw.polygon(screen, GREEN, [[375, 5], [415, 25], [375, 45]], 5)
                if worldModel.startTime == 0:
                    worldModel.startTime = getTimeInMilliseconds()




            #player hitpoints
            hitPoints = worldModel.hitPoints
            while hitPoints > 0:
                pygame.draw.rect(screen,RED,pygame.Rect(10+20*hitPoints,480,5,10))
                hitPoints = hitPoints - 1



            #display
            allsprites.draw(screen)
            pygame.display.flip()



# This isn't run on Android.
if __name__ == "__main__":
    main()
