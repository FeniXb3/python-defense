'''
@author: Piotrek
'''

import math
from Engine.EnemyModel import EnemyModel
from Engine.AmmoModel import AmmoModel
from Engine.TowerModel import TowerModel
from Engine.WaveModel import WaveModel

class PathPoint(object):
    x = 0
    y = 0
    isGate = False
    def __init__(self, x, y, isGate = False):
        self.x = x
        self.y = y
        self.isGate = isGate


class WorldModel(object):
    enemies = []
    waves = []
    towers = []
    pathPoints = []
    ammoObjects = []
    playerPoints = 0
    gameOver = False
    hitPoints = 10
    startTime = 0
    maxTowers = 10
    
        
    
    def __calculateDistanceAfterTime(self, milliseconds, speed):
        return speed*(milliseconds/1000.0)

    def __calculateDistance(self, startX, startY, endX, endY):
        x = endX - startX
        y = endY - startY
        return math.sqrt(x*x + y*y)
    
    def __moveEnemyByDistance(self, enemyRange, enemy):
        assert isinstance(enemy, EnemyModel)
        nextPP = self.pathPoints[enemy.nextPathPoint]
        assert isinstance(nextPP, PathPoint)
        #go to next PathPoint
        distance = self.__calculateDistance(enemy.positionX, enemy.positionY, nextPP.x, nextPP.y)
        #print "distance: ", distance
        if distance > enemyRange:
            #print "distance > enemyRange"
            #new location           
            newDistance = distance - enemyRange   
            distanceX = nextPP.x-enemy.positionX
            distanceY = nextPP.y-enemy.positionY
            enemy.positionX += distanceX - newDistance*distanceX/distance 
            enemy.positionY += distanceY - newDistance*distanceY/distance 
            enemy.rect.center = enemy.positionX, enemy.positionY
            

            deltaY = enemy.positionY - nextPP.y
            deltaX = enemy.positionX - nextPP.x
            angleInDegrees = math.degrees(math.atan2(deltaY, deltaX))
            enemy.turn(angleInDegrees)

        elif nextPP.isGate == True:
            self.hitPoints = self.hitPoints -1
            if self.hitPoints <= 0:
                #game over
                self.gameOver = True
            print enemy.waveNumber
            self.waves[enemy.waveNumber].enemies.remove(enemy)
            del enemy
        else:
            #move enemy to next point
            enemy.positionX = nextPP.x
            enemy.positionY = nextPP.y
            enemy.rect.center = enemy.positionX, enemy.positionY
            #go to next path point
            currentPathPointId = self.pathPoints.index( nextPP )
            enemy.nextPathPoint = currentPathPointId+1
            self.__moveEnemyByDistance(enemyRange-distance, enemy)

            
            
    
    def __moveEnemyByTime(self, deltaTime, enemy):
        assert isinstance(enemy, EnemyModel)
        enemyRange = self.__calculateDistanceAfterTime(deltaTime, enemy.speed)
        self.__moveEnemyByDistance(enemyRange, enemy)
       

    def __moveSingleAmmo(self, deltaTime, ammo):
        assert isinstance(ammo, AmmoModel)
        if (isinstance(ammo.target, EnemyModel) == False):
            self.ammoObjects.remove(ammo)
            #del ammo
            return

        if not ammo.doUpdate:

            assert isinstance(ammo.target, EnemyModel)
            #does enemy still exist?
            try:

                #is it a hit?
                distance = self.__calculateDistance(ammo.positionX, ammo.positionY, ammo.target.positionX, ammo.target.positionY)
                ammoRange = self.__calculateDistanceAfterTime(deltaTime, ammo.speed)
                if not self.__isInRange(ammo.positionX, ammo.positionY, ammo.source.fireRange, ammo.source.positionX, ammo.source.positionY):
                    self.ammoObjects.remove(ammo) 
                    del ammo  
                    return
                elif distance > ammoRange:
                    #new location           
                    newDistance = distance - ammoRange   
                    distanceX = ammo.target.positionX-ammo.positionX
                    distanceY = ammo.target.positionY-ammo.positionY
                    ammo.positionX += distanceX - newDistance*distanceX/distance
                    ammo.positionY += distanceY - newDistance*distanceY/distance


                    ammo.rect.topleft = ammo.positionX, ammo.positionY



                    deltaY = ammo.positionY - ammo.target.positionY
                    deltaX = ammo.positionX - ammo.target.positionX
                    angleInDegrees = math.degrees(math.atan2(deltaY, deltaX))
                    ammo.turn(angleInDegrees)

                else:
                    #hit
                    ammo.hit()
                    if ammo.target.hit( ammo.damage) <= 0 :
                        #remove target
                        self.playerPoints += ammo.target.hitpoints
                        self.waves[ammo.target.waveNumber].enemies.remove(ammo.target)
                        if len(self.waves[ammo.target.waveNumber].enemies) == 0:
                            self.maxTowers = self.maxTowers * 2
                            for tower in self.towers:
                                tower.upgrade()
                        for rest in self.ammoObjects:
                            assert isinstance(rest, AmmoModel)
                            if rest.target == ammo.target and rest!=ammo:
                                self.ammoObjects.remove(rest) 
                                del rest 
                        del ammo.target
                    else:
                        pass
                        #self.ammoObjects.remove(ammo) 
                        #ammo.source.ammoSprites.remove(ammo)
                        #del ammo              
            except ValueError:
            #enemy does not exist, remove ammo unit
                self.ammoObjects.remove(ammo)
                #del ammo
        
        ammo.update()
        if ammo.destroy:
            self.ammoObjects.remove(ammo)

    def __isInRange(self, centerX, centerY, rangeDistance, pointX, pointY):
        if rangeDistance >= self.__calculateDistance(centerX, centerY, pointX, pointY):
            return True
        else:
            return False

    def __getEnemyInRange(self, centerX, centerY, rangeDistance):
        enemyInRange = False
        enemyNextPathPointId = 0
        enemyDistanceToNextPathPoint = 0
            

        for wave in self.waves:
            for enemy in wave.enemies:
                assert isinstance(enemy, EnemyModel)
                nextPP = self.pathPoints[enemy.nextPathPoint]
                assert isinstance(nextPP, PathPoint)
                if self.__isInRange(enemy.positionX, enemy.positionY, rangeDistance, centerX, centerY):
                    enemyNextPathPointIdTemp = self.pathPoints.index( nextPP )
                    enemyDistanceToNextPathPointTemp = self.__calculateDistance(enemy.positionX, enemy.positionY, nextPP.x, nextPP.y)
                    if (enemyInRange == False) or (enemyNextPathPointId < enemyNextPathPointIdTemp) or (enemyDistanceToNextPathPoint > enemyDistanceToNextPathPointTemp):
                        enemyNextPathPointId = enemyNextPathPointIdTemp
                        enemyDistanceToNextPathPoint = enemyDistanceToNextPathPointTemp
                        enemyInRange = enemy
        return enemyInRange
            

    def __fireFromTower(self, tower, deltaTime):
        assert isinstance(tower, TowerModel)
        
        enemy = self.__getEnemyInRange(tower.positionX, tower.positionY, tower.fireRange)
        if enemy == False:
            tower.isShooting = False
            tower.timeForFire += deltaTime
            if(tower.timeForFire > 1000/tower.speed):
                tower.timeForFire = 1000/tower.speed
        else:
            tower.isShooting = True

            deltaY = tower.positionY - enemy.positionY
            deltaX = tower.positionX - enemy.positionX
            angleInDegrees = math.degrees(math.atan2(deltaY, deltaX))
            tower.turn(angleInDegrees)

            tower.timeForFire += deltaTime
            #print tower.timeForFire, " ", deltaTime
            if (tower.timeForFire < 1000/tower.speed):
                return   
            
            missle = tower.fire(enemy)
            self.ammoObjects.append(missle)
            tower.timeForFire = 0
                #move a bit
            if tower.timeForFire>0:
                self.__moveSingleAmmo(tower.timeForFire, missle)
        
    def fireTowers(self, deltaTime):
        for tower in self.towers:
            self.__fireFromTower(tower, deltaTime)

    #deltaTime in milliseconds
    def moveAmmo(self, deltaTime):
        for ammo in self.ammoObjects:
            self.__moveSingleAmmo(deltaTime, ammo)
            
    def moveEnemies(self, deltaTime):

        for wave in self.waves:
            if wave.started == True:
                for enemy in wave.enemies:
                    self.__moveEnemyByTime(deltaTime, enemy)
        
    