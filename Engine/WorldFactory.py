'''
@author: Piotrek
'''
from Engine.WorldModel import WorldModel
from Engine.WorldModel import PathPoint
from Engine.TowerModel import TowerModel
from Engine.AmmoModel import AmmoModel
from Engine.EnemyModel import EnemyModel
from Engine.WaveModel import WaveModel

from random import random

class WorldFactory(object):

    def createWorld(self):
        #path
        world = WorldModel()
        point = PathPoint(0,0) # PathPoint(random()*400, random()*450)
        world.pathPoints.append(point)

        pointsCount = 5#random()*20+5

        while pointsCount > 0:

            if pointsCount > 1:

                point = PathPoint(random()*400+20, random()*450+20)
            else:
                point = PathPoint(random()*400+20, random()*450+20, True)
                #point.isGate = True

            pathLen = len(world.pathPoints)
            if pathLen <= 3:
                world.pathPoints.append(point)
                pointsCount = pointsCount - 1
            elif pathLen > 3:
                epsilon = 25
                index = 0
                insertPoint = True
                while index < pathLen-2:
                    insertPoint = not self.intersect(world.pathPoints[index], world.pathPoints[index + 1], world.pathPoints[pathLen - 1], point)
                    if not insertPoint:
                        break
                    index = index + 1
                if insertPoint:
                    world.pathPoints.append(point)
                    pointsCount = pointsCount - 1

       
        #waves
        for x in xrange(0,3):
            wave = WaveModel(25*x, (x+1)*2, x)
            world.waves.append(wave)
        # for x in xrange(0,1):
        #     wave = WaveModel(25*x, (x+1)*1, x)
        #     world.waves.append(wave)

        #print len(world.waves)

        return world


    def ccw(self, A,B,C):
        return (C.y-A.y) * (B.x-A.x) > (B.y-A.y) * (C.x-A.x)

    # Return true if line segments AB and CD intersect
    def intersect(self, A,B,C,D):
        return self.ccw(A,C,D) != self.ccw(B,C,D) and self.ccw(A,B,C) != self.ccw(A,B,D)