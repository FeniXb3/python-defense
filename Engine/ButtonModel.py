'''
@author: FeniX
'''

import pygame

class Button(pygame.sprite.Sprite):

    pressed = False
    pressedsurf = None
    notpressedsurf = None

    def __init__(self, top, left, width, height, pressedsurf, notpressedsurf):
    # Call the parent class (Sprite) constructor
        pygame.sprite.Sprite.__init__(self)

        self.notpressedsurf = notpressedsurf
        self.pressedsurf = pressedsurf
        
        self.image = notpressedsurf
        self.rect = self.image.get_rect()

        self.rect.topleft = top, left

    def press(self):

        self.pressed = not self.pressed
        if self.pressed:
            self.image = self.pressedsurf
        else:
            self.image = self.notpressedsurf